import numpy as np
import netCDF4 as nc4

# AGCM fractional land mask (ocean fraction=1)
nc = nc4.Dataset('t63_x_srcf.nc','r')
sftof = nc.variables['SRCF'][:]
nc.close()  

# AGCM grid cell areas, from CMIP5/CanESM2 (should not have changed)
nc = nc4.Dataset('areacella_fx_CanESM2_historical-r1_r0i0p0.nc','r')
areacella =  nc.variables['areacella'][:]                                
nc.close()

# NEMO mesh mask
nc = nc4.Dataset('nemo3.4.1_orca1_mesh_mask_ngr.nc.001','r')
tmask = nc.variables['tmask'][:] # binary ocean mask, ocean=1
e1t = nc.variables['e1t'][:]     # longitude box lengths
e2t = nc.variables['e2t'][:]     # latitude box lengths
nc.close()

# Compute the ocean-only areas
nemo_area_ocn = np.sum(e1t * e2t * tmask[0,0,:,:].squeeze())
agcm_area_ocn = np.sum(sftof*areacella)

print "Ocean areas, (NEMO, AGCM) then %"
print nemo_area_ocn/1e14, agcm_area_ocn/1e14
print (nemo_area_ocn/agcm_area_ocn)*100.

# Total grid areas
nemo_area = np.sum(e1t*e2t)
agcm_area = np.sum(areacella)
print "Total areas, (NEMO, AGCM) then %"
print nemo_area/1e14, agcm_area/1e14
print (nemo_area/agcm_area)*100.


