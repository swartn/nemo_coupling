import matplotlib.pyplot as plt
plt.ion()
plt.close('all')
import numpy as np
import cmipdata as cd

def add_cbar(cot, ax, units):
    box = ax.get_position()
    tl = fig.add_axes([box.x0+box.width*1.035, box.y0, 0.01, box.height])
    #bounds = np.linspace(vmin, vmax, ncols)
    fig.colorbar(cot, cax=tl, label=units)

ifile='/home/ncs/ra40/nemo_out/coupled/odp/mc_odp_3h_00010701_00010731_grid_t.nc.001'
mfile = '/home/ncs/ra40/nemo_out/nemo_3.4_orca1_mesh_mask.nc'
dqns = cd.loadvar(ifile, 'dqns').mean(axis=0)
dqlw = cd.loadvar(ifile, 'dqlw').mean(axis=0)
dqsb = cd.loadvar(ifile, 'dqsb').mean(axis=0)
dqla = cd.loadvar(ifile, 'dqla').mean(axis=0)
sic = cd.loadvar(ifile, 'soicecov').mean(axis=0)
imask = sic
imask[imask>0] = 1.0
tmask = cd.loadvar(mfile, 'tmask')[0,:,:].squeeze()

fig, axa = plt.subplots(2,2, figsize=(8,8))
plt.subplots_adjust(wspace=0.35)

varl = [dqns, dqlw*-1, dqsb*-1, dqla*-1]

tits = ['Dqns/Dt', 'Dqlw/DT', 'Dqsb/DT', 'Dqla/DT']

for i, ax in enumerate(axa.flatten()):
    vp = np.ma.masked_equal(varl[i]*tmask*imask, 0)
    cot = ax.pcolormesh(vp, cmap='Blues_r', vmin=-45, vmax=0, rasterized=True)
    ax.text(200,175, str(np.round(vp.mean(),2)))
    ax.set_title(tits[i])
    print tits[i], vp.mean()

add_cbar(cot, axa[0,1], units='W m$^{-2}$ K$^{-1}$')
fig.savefig('DqnsDt_bulk_formula_run-odo.pdf')






