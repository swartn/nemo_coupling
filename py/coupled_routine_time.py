""" Compares NEMO routine timing in a coupled run and a bulk run
"""

import numpy as np
import matplotlib.pyplot as plt
from collections import OrderedDict
plt.close('all')
plt.ion()


f1 = 'mike101_nemo_routine_timing.txt'
f2 = 'odp_nemo_routine_timing.txt'

dn1 = np.loadtxt(f1, dtype={'names':
                 ('routines', 'times'),'formats': ('S11','f4')}, delimiter=','
                 , skiprows=1)

dn2 = np.loadtxt(f2, dtype={'names':
                 ('routines', 'times'),'formats': ('S11','f4')}, delimiter=','
                 , skiprows=1)


fig, (axt) = plt.subplots(1,1, figsize=(8,8))
fig.subplots_adjust(bottom=0.2)

nr = 20
for i in range(nr):
    rn1 = dn1['routines'][i]
    ind_n2 = np.where( dn2['routines'] == rn1 )[0]

    axt.semilogy(i, dn1['times'][i], 'ko', label='coupled')
    if ind_n2.size > 0:
        axt.semilogy(i, dn2['times'][ind_n2], 'ro', label='bulk')

axt.set_xticks(range(nr))
axt.set_xticklabels(dn1['routines'][0:nr], rotation='vertical')
axt.set_ylabel('Routine time (s)')

handles, labels = axt.get_legend_handles_labels()
by_label = OrderedDict(zip(labels, handles))
axt.legend(by_label.values(), by_label.keys(), frameon=False, numpoints=1)

plt.savefig('NEMO-timing-by-routine.png', bbox_inches='tight', dpi=300)




